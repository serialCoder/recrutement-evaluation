package com._4dconcept.evaluation.model.enums;

public enum ProjectStatus {

    ENABLED,

    DISABLED,

    SINGLE_ACTIVE,

    SINGLE_INACTIVE,

    ALL_INACTIVE,

    MAJORITY_INACTIVE,

    AT_LEAST_ONE_INACTIVE,

    NO_INACTIVE

}
