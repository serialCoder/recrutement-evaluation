package com._4dconcept.evaluation.model.enums;

public enum DeveloperStatus {
    ACTIVE,
    INACTIVE
}
