package com._4dconcept.evaluation.model;

import com._4dconcept.evaluation.model.enums.DeveloperStatus;
import lombok.*;
import org.hibernate.proxy.HibernateProxy;

import javax.persistence.*;
import java.util.Objects;

@Entity
@SequenceGenerator(name = Developer.DEVELOPER_SEQ, sequenceName = Developer.DEVELOPER_SEQ, allocationSize = 100)
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Developer {

    protected static final String DEVELOPER_SEQ = "developer_seq";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = DEVELOPER_SEQ)
    private Long id;
    private String name;
    @Enumerated(EnumType.STRING)
    private DeveloperStatus status;
    @OneToOne
    private Project project;

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        Class<?> oEffectiveClass = o instanceof HibernateProxy ? ((HibernateProxy) o).getHibernateLazyInitializer().getPersistentClass() : o.getClass();
        Class<?> thisEffectiveClass = this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass() : this.getClass();
        if (thisEffectiveClass != oEffectiveClass) return false;
        Developer developer = (Developer) o;
        return getId() != null && Objects.equals(getId(), developer.getId());
    }

    @Override
    public final int hashCode() {
        return this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass().hashCode() : getClass().hashCode();
    }
}
