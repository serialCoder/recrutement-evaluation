package com._4dconcept.evaluation.batch.bean;

import javax.xml.bind.annotation.*;
import java.util.HashSet;
import java.util.Set;

@XmlRootElement(name = "project")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProjectData {

    @XmlElement
    public Long id;
    @XmlElement
    public String name;
    @XmlElement
    public String status;

    @XmlElementWrapper(name = "tags")
    @XmlElement(name = "tag")
    public Set<String> tags = new HashSet<>();
}
