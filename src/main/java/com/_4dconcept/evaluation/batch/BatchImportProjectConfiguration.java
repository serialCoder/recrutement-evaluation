package com._4dconcept.evaluation.batch;

import com._4dconcept.evaluation.batch.bean.ProjectData;
import com._4dconcept.evaluation.rest.mapper.ProjectMapper;
import com._4dconcept.evaluation.services.ProjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.xml.StaxEventItemReader;
import org.springframework.batch.item.xml.builder.StaxEventItemReaderBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
@RequiredArgsConstructor
public class BatchImportProjectConfiguration {


    public static final String ROOT_ELEMENT = "project";
    private static final int CHUNK_SIZE = 100;
    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;
    private final ProjectService projectService;
    private final ProjectMapper projectMapper;

    @Value("classpath:projects.xml")
    Resource projectsData;

    @Bean
    public Job importProject(@Qualifier("xmlDataImportStep") Step xmlDataImportStep) {
        return jobBuilderFactory.get("importProject").start(xmlDataImportStep).build();
    }

    @Bean
    public Step xmlDataImportStep() {
        return stepBuilderFactory.get("xmlDataImportStep").<ProjectData, ProjectData>chunk(CHUNK_SIZE)
                .reader(projectDataReader())
                .writer(projectDataItemWriter())
                .build();
    }


    @Bean
    public StaxEventItemReader<ProjectData> projectDataReader() {
        Jaxb2Marshaller projectDataMarshaller = new Jaxb2Marshaller();
        projectDataMarshaller.setClassesToBeBound(ProjectData.class);

        return new StaxEventItemReaderBuilder<ProjectData>()
                .unmarshaller(projectDataMarshaller)
                .name("projectDataReader")
                .resource(projectsData)
                .addFragmentRootElements(ROOT_ELEMENT)
                .build();
    }

    @Bean
    public ItemWriter<ProjectData> projectDataItemWriter() {
        return new ProjectDataWriter(projectService, projectMapper);
    }
}
