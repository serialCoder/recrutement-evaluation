package com._4dconcept.evaluation.batch;

import com._4dconcept.evaluation.batch.bean.ProjectData;
import com._4dconcept.evaluation.model.Project;
import com._4dconcept.evaluation.rest.mapper.ProjectMapper;
import com._4dconcept.evaluation.services.ProjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.item.ItemWriter;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ProjectDataWriter implements ItemWriter<ProjectData> {

    private final ProjectService projectService;
    private final ProjectMapper projectMapper;

    @Override
    public void write(List<? extends ProjectData> items) {
        List<Project> projectList = items.stream().map(projectMapper::toEntity).collect(Collectors.toList());
        projectService.createOrUpdateProjects(projectList);
    }
}
