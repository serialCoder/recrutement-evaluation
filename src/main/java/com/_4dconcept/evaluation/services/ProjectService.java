package com._4dconcept.evaluation.services;


import com._4dconcept.evaluation.model.Project;
import com._4dconcept.evaluation.repository.ProjectRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProjectService {
    private final ProjectRepository projectRepository;


    /**
     * Save news projects or update existing one
     *
     * @param projects the projects to save or update
     * @return the created or updated projects
     */
    public List<Project> createOrUpdateProjects(List<Project> projects) {
        return projectRepository.saveAll(projects);
    }
}
