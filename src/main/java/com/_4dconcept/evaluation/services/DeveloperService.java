package com._4dconcept.evaluation.services;

import com._4dconcept.evaluation.model.Developer;
import com._4dconcept.evaluation.model.Project;
import com._4dconcept.evaluation.repository.DeveloperRepository;
import com._4dconcept.evaluation.repository.ProjectRepository;
import com._4dconcept.evaluation.repository.specification.DeveloperSpec;
import com._4dconcept.evaluation.rest.Filter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DeveloperService {
    private final DeveloperRepository developerRepository;
    private final ProjectRepository projectRepository;

    /**
     * Save new developer or update existing one
     *
     * @param developer the developer to save or update
     * @return the created or updated developer
     */
    public Developer createDeveloper(Developer developer) {
         Project project = null;
        if (!Objects.isNull(developer.getProject()) && !Objects.isNull(developer.getProject().getId())) {
            project = projectRepository.findById(developer.getProject().getId()).orElse(project);
        }
        developer.setProject(project);
        return developerRepository.saveAndFlush(developer);
    }

    /**
     * Search developers matching with filter
     *
     * @param filter the filter to apply
     * @return list of developers matching with filter
     */
    public List<Developer> findByFilter(Filter filter) {
        DeveloperSpec developerSpec = new DeveloperSpec();
        return developerRepository.findAll(developerSpec.buildSpecification(filter));
    }
}
