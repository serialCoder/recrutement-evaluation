package com._4dconcept.evaluation.repository.specification;

import com._4dconcept.evaluation.model.Developer;
import com._4dconcept.evaluation.rest.Filter;
import org.springframework.data.jpa.domain.Specification;

public class DeveloperSpec {
    private static final String PROJECT = "project";

    /**
     * Create specification which is always true (1 = 1)
     *
     * @return always true specification
     */
    private Specification<Developer> emptySec() {
        return ((root, query, criteriaBuilder) -> criteriaBuilder.conjunction());
    }

    /**
     * Build the specification to apply to search
     *
     * @param filter the filter to applu
     * @return all specification to apply to search
     */
    public Specification<Developer> buildSpecification(Filter filter) {
        Specification<Developer> developerSpecification = emptySec();
        if (!filter.isAll()) {
            developerSpecification = developerSpecification.and(onlyProjectDev());
        }
        return developerSpecification;
    }

    /**
     * Create specification for dev linked with project
     *
     * @return specification
     */
    private Specification<Developer> onlyProjectDev() {
        return ((root, query, criteriaBuilder) -> criteriaBuilder.isNotNull(root.get(PROJECT)));
    }
}
