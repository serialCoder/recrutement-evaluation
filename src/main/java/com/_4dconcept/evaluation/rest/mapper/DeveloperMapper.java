package com._4dconcept.evaluation.rest.mapper;

import com._4dconcept.evaluation.model.Developer;
import com._4dconcept.evaluation.rest.dto.DeveloperDTO;
import org.mapstruct.*;

@Mapper(componentModel = DeveloperMapper.SPRING_COMPONENT_MODEL, builder = @Builder, nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE, nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface DeveloperMapper {
    String SPRING_COMPONENT_MODEL = "spring";

    @Mapping(target = "projectName", source = "project.name")
    DeveloperDTO toDTO(Developer developer);

    @Mapping(source = "projectName", target = "project.name")
    @Mapping(source = "projectId", target = "project.id")
    Developer toCreateEntity(DeveloperDTO developerDTO);

}
