package com._4dconcept.evaluation.rest.mapper;

import com._4dconcept.evaluation.batch.bean.ProjectData;
import com._4dconcept.evaluation.model.Project;
import com._4dconcept.evaluation.model.enums.ProjectStatus;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(componentModel = ProjectMapper.SPRING_COMPONENT_MODEL)
public interface ProjectMapper {
    String SPRING_COMPONENT_MODEL = "spring";

    @Named("convertToStatus")
    static ProjectStatus convertToStatus(String statusToConvert) {
        return ProjectStatus.valueOf(statusToConvert.toUpperCase());
    }

    @Mapping(target = "status", qualifiedByName = "convertToStatus")
    Project toEntity(ProjectData projectData);
}
