package com._4dconcept.evaluation.rest.controller;

import com._4dconcept.evaluation.model.Developer;
import com._4dconcept.evaluation.rest.Filter;
import com._4dconcept.evaluation.rest.dto.DeveloperDTO;
import com._4dconcept.evaluation.rest.mapper.DeveloperMapper;
import com._4dconcept.evaluation.services.DeveloperService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/developers", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class RestApiController {

    private final DeveloperService developerService;

    private final DeveloperMapper developerMapper;

    @GetMapping
    public List<DeveloperDTO> retrieveDeveloppers(Filter filter) {
        List<Developer> developers = developerService.findByFilter(filter);
        return developers.stream().map(developerMapper::toDTO).collect(Collectors.toList());
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public DeveloperDTO createDeveloper(@RequestBody DeveloperDTO developerDTO) {
        Developer developer = developerMapper.toCreateEntity(developerDTO);
        return developerMapper.toDTO(developerService.createDeveloper(developer));
    }
}
