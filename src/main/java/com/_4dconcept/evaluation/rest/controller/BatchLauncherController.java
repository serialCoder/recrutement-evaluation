package com._4dconcept.evaluation.rest.controller;


import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/job")
public class BatchLauncherController {

    private final JobLauncher jobLauncher;
    private final Job importProjectJob;

    public BatchLauncherController(JobLauncher jobLauncher, Job importProjectJob) {
        this.jobLauncher = jobLauncher;
        this.importProjectJob = importProjectJob;
    }

    @GetMapping
    public String runImportJob() throws JobInstanceAlreadyCompleteException, JobExecutionAlreadyRunningException, JobParametersInvalidException, JobRestartException {
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("launchTime", LocalDateTime.now().toString());
        JobExecution run = jobLauncher.run(importProjectJob, jobParametersBuilder.toJobParameters());
        return run.getStatus().name();

    }
}
