package com._4dconcept.evaluation.rest;

import lombok.Data;

@Data
public class Filter {
    private boolean all = true;
}
