package com._4dconcept.evaluation.rest.dto;

import lombok.Data;

@Data
public class DeveloperDTO {
    private Long id;
    private String name;
    private Long projectId;
    private String projectName;
}
