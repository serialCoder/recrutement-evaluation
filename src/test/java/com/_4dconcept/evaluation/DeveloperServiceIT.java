package com._4dconcept.evaluation;

import com._4dconcept.evaluation.model.Developer;
import com._4dconcept.evaluation.model.enums.DeveloperStatus;
import com._4dconcept.evaluation.services.DeveloperService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@Transactional
public class DeveloperServiceIT {

    @Autowired
    private DeveloperService developerService;

    @Test
    public void createDeveloperWithNoProject() throws Exception {
        // GIVEN
        Developer caso = Developer.builder()
                .name("caso")
                .status(DeveloperStatus.ACTIVE)
                .build();
        // WHEN
        Assertions.assertNull(caso.getId(), "id should be null because entity is not saved yet");
        Developer developerCreated = developerService.createDeveloper(caso);
        // THEN
        Assertions.assertNull(caso.getProject(), "this developer is not associated with project, project should be null");
        Assertions.assertNotNull(developerCreated.getId(), "After registration in database, we should not avoid id as null anymore.");
        Assertions.assertNull(developerCreated.getProject(), "Even after registration, project should still be null because cascade on save is not defined");

    }

}
